import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageErroeComponent } from './pages/page-erroe/page-erroe.component';
import { PageHomeComponent } from './pages/page-home/page-home.component';
import { PAgeInfoComponent } from './pages/page-info/page-info.component';
import { PageSobreComponent } from './pages/page-sobre/page-sobre.component';

const routes: Routes = [
  { path: '', component: PageHomeComponent, pathMatch: 'full' },
  { path: 'sobre', component: PageSobreComponent, children: [
    { path: 'rodolfo', component: PageSobreComponent } // Rota filha
  ]},
  { path: '404', component: PageErroeComponent }, // Rota de erro
  { path: 'info/:id', component: PAgeInfoComponent },
  { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(mod => mod.DashboardModule)}, // Lazy loading do módulo.
  { path: '**', redirectTo: '404' }, // Rota coringa com redirecionamento / Necessário estar em último.
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
