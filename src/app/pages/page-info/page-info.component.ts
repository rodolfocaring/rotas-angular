import { Component, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-page-info',
  templateUrl: './page-info.component.html',
  styleUrls: ['./page-info.component.scss']
})
export class PAgeInfoComponent implements OnInit {
  private params: string = 'bola-cheia';
  
  constructor(
    private activedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    // this.activedRoute.params.subscribe(res => console.log(res)) // query params; 
    // this.activedRoute.queryParams.subscribe(res => console.log(res)) // path params;
    this.activedRoute.params.subscribe(res => this.params = res['id']);
    console.log(this.activedRoute.snapshot.params['id']); // Forma mais fácil de identificar o params inserido.

    if (this.params === 'home') {
      this.goToHome();
    } else if (this.params === 'sobre') {
      this.goToUrl();
    }
  }

  goToHome(): void {
    this.router.navigate(['/']); // Redirecionamento sem reload da página.
  }

  goToUrl(): void {
    this.router.navigateByUrl('/sobre'); // Redirecionamento com reload.
  }
}