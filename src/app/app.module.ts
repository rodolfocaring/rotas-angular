import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageHomeComponent } from './pages/page-home/page-home.component';
import { PageSobreComponent } from './pages/page-sobre/page-sobre.component';
import { PageErroeComponent } from './pages/page-erroe/page-erroe.component';
import { MenuComponent } from './components/menu/menu.component';
import { PAgeInfoComponent } from './pages/page-info/page-info.component';
import { DashboardRoutingModule } from './dashboard/dashboard-routing.module';
import { DashboardModule } from './dashboard/dashboard.module';

@NgModule({
  declarations: [
    AppComponent,
    PageHomeComponent,
    PageSobreComponent,
    PageErroeComponent,
    MenuComponent,
    PAgeInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DashboardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
